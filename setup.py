from setuptools import find_packages, setup

module_name = "pymapeditor"

setup(
    name=module_name,
    version="2.0",
    description="Tilemap editor for 2D game",
    author="Severi Kujala",
    packages=find_packages(where="src"),
    package_dir={"": "src"},

    entry_points={
    },

    install_requires=[
        "PyQt5==5.15.4",
        "PyQt5-Qt5==5.15.2",
        "PyQt5-sip==12.8.1",
        "toml==0.10.2",
    ],

    extras_require={
        "test": [
            "pytest",
        ],
    },

)

