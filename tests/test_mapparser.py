import os
import sys

from src.mapparser import writeMap, readMap
from src.tile import Tile


def test_write_and_read():

    PATH = "./tests/out.csv"
    tilesOut = []
    w = 4
    h = 4

    for j in range(h):

        for i in range(w):
            tile = Tile(i, j)
            tile.destroyable = False
            tilesOut.append(tile)


    # first row
    for i in range(w):
        tilesOut[i].type = "grassland"

    tilesOut[0].items = ["item1", "item2", "item3"]

    tilesOut[-1].traversability = 0
    tilesOut[-1].destroyable = True

    #tilesOut = readMap("./tests/test.csv")


    writeMap(PATH, tilesOut)

    tilesIn = readMap(PATH)


    assert len(tilesIn) != 0

    for i in range(w*h):
        assert tilesIn[i].x == tilesOut[i].x
        assert tilesIn[i].y == tilesOut[i].y
        assert tilesIn[i].type == tilesOut[i].type
        assert tilesIn[i].traversability == tilesOut[i].traversability
        assert tilesIn[i].destroyable == tilesOut[i].destroyable

        assert len(tilesIn[i].items) == len(tilesOut[i].items)

        for j in range( len(tilesIn[i].items) ):
            assert tilesIn[i].items[j] == tilesOut[i].items[j]










