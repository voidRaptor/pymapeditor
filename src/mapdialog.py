from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QFormLayout
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QSpinBox
from PyQt5.QtWidgets import QCheckBox
from PyQt5.QtWidgets import QGroupBox


class MapDialog(QDialog):
    """Dialog for various settings for a new map."""


    def __init__(self, parent=None):
        super().__init__(parent)

        self.mapSize = None

        self.setWindowTitle("New Map")

        self.mapWSpinbox = QSpinBox()
        self.mapWSpinbox.setMinimum(1)
        self.mapWSpinbox.setMaximum(1000)
        self.mapWSpinbox.setValue(10)

        self.mapHSpinbox = QSpinBox()
        self.mapHSpinbox.setMinimum(1)
        self.mapHSpinbox.setMaximum(1000)
        self.mapHSpinbox.setValue(10)

        self.ratioCheckbox = QCheckBox("Keep aspect ratio")
        self.ratioCheckbox.setChecked(True)

        mapLay = QFormLayout()
        mapLay.addRow(QLabel("Map size (tiles)"))
        mapLay.addRow(self.mapWSpinbox, QLabel("width"))
        mapLay.addRow(self.mapHSpinbox, QLabel("height"))
        mapLay.addRow(self.ratioCheckbox)
        mapGroup = QGroupBox()
        mapGroup.setLayout(mapLay)

        cancelButton = QPushButton("Cancel")
        okButton = QPushButton("Ok")

        buttonLay = QHBoxLayout()
        buttonLay.addWidget(cancelButton)
        buttonLay.addWidget(okButton)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(mapGroup)
        mainLayout.addLayout(buttonLay)
        self.setLayout(mainLayout)

        cancelButton.pressed.connect(self.reject)
        okButton.pressed.connect(self.accept)
        self.mapWSpinbox.valueChanged.connect(self.keepAspectRatioH)
        self.mapHSpinbox.valueChanged.connect(self.keepAspectRatioW)

        self.show()


    def keepAspectRatioW(self, value):
        """When the checkbox is checked, values are mirrored to the other spinbox."""

        if self.ratioCheckbox.isChecked():
            self.mapWSpinbox.setValue(value)


    def keepAspectRatioH(self, value):
        """When the checkbox is checked, values are mirrored to the other spinbox."""

        if self.ratioCheckbox.isChecked():
            self.mapHSpinbox.setValue(value)


    def accept(self):
        """Handler for signal emitted when closing the dialog."""

        self.mapSize = (self.mapWSpinbox.value(), self.mapHSpinbox.value())
        super().accept()


