from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QUndoStack

from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QKeySequence

from PyQt5.QtCore import QSize
from PyQt5.QtCore import Qt
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtCore import QObject

import os
import logging

from tilelistitem import TileListItem
from mapdialog import MapDialog
from mapscene import MapScene
from texturedatabase import TextureDatabase
from tilegraphicsitem import TileGraphicsItem
from tilecommand import TileCommand
from mainwindow import MainWindow

from tile import Tile
from tile import Texture

from utility import Tool
from utility import getFileName
from utility import showErrorMessage

import mapparser


class MainController(QObject):
    """General logic handler for the application."""


    def __init__(self):
        """Constructor

        :attr saved: bool, whether map should be saved
        :attr saveFile: str, path to save file
        :attr chosenTool: Tool, currently used tool
        :attr *Path: str, paths for important directories used in application
        :attr undoStack: QUndoStack, undo/redo handler
        :attr undoAction: QAction
        :attr redoAction: QAction

        """
        super().__init__()

        self.saved = True # no need for saving when no map
        self.saveFile = None
        self.chosenTool = Tool.PLACER

        runPath = os.getcwd()
        self.projectDir = runPath
        split = runPath.split("/")

        if split[-1]  == "src":
            self.projectDir = "/".join(split[0:-1])

        self.projectDir += "/"
        self.resPath = self.projectDir + "res/"
        self.mapPath = self.projectDir + "maps/"
        self.tilePath = self.resPath + "tiles/"
        self.iconPath = self.resPath + "icons/"

        logFile = self.projectDir + "mapeditor.log"
        logFormat = "%(asctime)s %(name)s %(levelname)s %(module)s.%(funcName)s:%(lineno)s - %(message)s"
        logDateTimeFormat = "%Y-%m-%d %H:%M:%S"
        logging.basicConfig(filename=logFile, format=logFormat, datefmt=logDateTimeFormat, level=logging.DEBUG)

        self.undoStack = QUndoStack(self)
        self.undoAction = self.undoStack.createUndoAction(self, "Undo")
        self.undoAction.setShortcuts(QKeySequence.Undo)
        self.redoAction = self.undoStack.createRedoAction(self, "Redo")
        self.redoAction.setShortcuts(QKeySequence.Redo)

        self.texDb = TextureDatabase()
        self.mapScene = MapScene(self)
        self.window = MainWindow(self)

        self.mapScene.mapChanged.connect(self.newChangesMade)

        self.loadDefaultTextures()


    def clearLoadedTextures(self):
        """Clears unused textures from the texture list and database."""

        count = self.window.textureList.count()
        used = self.mapScene.getUsedTextures()

        for i in range(count):
            item = self.window.textureList.item(count - i - 1)

            if item.name not in used:
                self.window.textureList.takeItem(count - i - 1)
                self.texDb.remove(item.name)


    def newMap(self):
        """Create new map based on settings set by user.

        If the map currently being edited is not saved, a dialog, asking whether
        to the changes should be saved, will be opened.

        """
        if not self.saved:
            self.saveMap()

        dialog = MapDialog(self.window)

        if dialog.exec() != QDialog.Accepted:
            return

        self.saveFile = None
        self.window.tileInfo.setText(f"{self.mapScene.cellSize[0]}x{self.mapScene.cellSize[1]}")
        self.window.mapInfo.setText(f"{dialog.mapSize[0]}x{dialog.mapSize[1]}")

        self.mapScene.setMapData(dialog.mapSize[0], dialog.mapSize[1])
        self.mapScene.createEmptyGrid()

        self.window.initLeftPanel()
        self.window.minimap.setMapData(dialog.mapSize, self.mapScene.cellSize)


    def loadTextures(self):
        """Load tile textures via QFileDialog and add them to MainWindow's texture list."""

        files = QFileDialog.getOpenFileNames(self.window, "Open File(s)", self.tilePath, "Images (*.png *.jpg)")[0]

        for f in files:
            name, pixmap = self.texDb.loadTile(f)

            if name is None or pixmap is None:
                text = f"Could not load textures in {self.tilePath}"
                continue

            self.window.textureList.addItem( TileListItem(name, pixmap) )


    @pyqtSlot(TileGraphicsItem, QPixmap, Tile)
    def newChangesMade(self, changedItem, oldPixmap, oldData):
        """Handler for signal emitted when there's a change in the map.

        This function creates undo/redo action. Window title is set to indicate
        changes.

        """
        self.undoStack.push( TileCommand(changedItem, oldPixmap, oldData) )

        self.saved = False

        if self.saveFile is None:
            self.window.setWindowTitle("untitled *")
        else:
            name = getFileName(self.saveFile)
            self.window.setWindowTitle(name +  " *")


    def saveMap(self):
        """Save map to a file and used textures in default texture directory.

        :return: bool, True if saving was successful

        If currently edited map is new, a file dialog for choosing save file
        will be openend.

        """
        if self.saveFile is None:
            return self.saveAs()

        mapparser.writeMap(self.saveFile, self.mapScene.getTiles())
        self.window.setWindowTitle(getFileName(self.saveFile) + " - saved")

        self.saved = True
        self.saveUsedTextures()

        return True


    def saveAs(self):
        """Save map to a file using file dialog and used textures in default directory.

        :return: bool
            True if saved or map doesn't need saving
            False if save file was not chosen

        """
        tiles = self.mapScene.getTiles()

        # no need to save, NOTE: empty maps (only default textures) can be saved
        if len(tiles) == 0:
            return True

        path = QFileDialog().getSaveFileName(self.window, "Save As", self.mapPath)[0]

        if path == "":
            return False

        self.saveFile = path
        mapparser.writeMap(self.saveFile, tiles)

        self.window.setWindowTitle(getFileName(path) + " - saved")

        self.saved = True
        self.saveUsedTextures()

        return True



    def loadMap(self):
        """Load map using file dialog and ask about saving changes.

        Default textures are loaded in case some of the textures needed by loaded
        map were removed by user.

        """
        # save first
        if not self.askSaveDialog():
            return

        path = QFileDialog.getOpenFileName(self.window, "Open File", self.mapPath, "*.csv")[0]
        tiles = mapparser.readMap(path)

        # cancel on file dialog
        if len(tiles) == 0:
            logging.info("load cancelled")
            return


        # in case unused in previous map got filtered out, but needed in the new
        self.loadDefaultTextures()

        # check if tiles in map can be found in db
        for t in tiles:

            if not self.texDb.contains(t.type):
                showErrorMessage(f"Failed to load map, which contains unknown tile type \"{t.type}\"")
                return

        self.mapScene.createMap(tiles, self.texDb)

        self.saveFile = path
        self.window.setWindowTitle(getFileName(path))

        # order of tiles is sorted by x,y -> max is last element
        tileCountX = tiles[-1].x+1
        tileCountY = tiles[-1].y+1

        self.window.initLeftPanel()
        self.window.minimap.setMapData((tileCountX, tileCountY), self.mapScene.cellSize)
        self.window.tileInfo.setText(f"{self.mapScene.cellSize[0]}x{self.mapScene.cellSize[1]}")
        self.window.mapInfo.setText(f"{tileCountX}x{tileCountY}")


    def chooseTilePlacer(self):
        """Handler for tool icon action."""

        self.chosenTool = Tool.PLACER


    def chooseTileEraser(self):
        """Handler for tool icon action."""

        self.chosenTool = Tool.ERASER


    def chooseViewMover(self):
        """Handler for tool icon action."""

        self.chosenTool = Tool.MOVER


    def loadDefaultTextures(self):
        """Loads textures from default texture directory to database and texture list."""

        files = os.listdir(self.tilePath)
        files = sorted(files)

        for f in files:

            if f[-3:].lower() != "png":
                logging.warning(f"{f}: only png images are allowed for autoload")
                continue

            name, pixmap = self.texDb.loadTile(self.tilePath + f)

            if name is None or pixmap is None:
                continue

            self.window.textureList.addItem( TileListItem(name, pixmap) )


    def saveUsedTextures(self):
        """Save textures used in current map into default texture directory."""

        used = self.mapScene.getUsedTextures()
        files = os.listdir(self.tilePath)

        # check which tiles used in map doesn't already exist in "/res/tiles"
        for name in used:

            if name + ".png" not in files:
                """
                pixmap = used[name].scaled(QSize(64, 64), Qt.KeepAspectRatio)
                pixmap.save(resPath + "/" + name + ".png", "png", -1)
                """
                used[name].save(self.tilePath + name + ".png", "png", -1)



    def askSaveDialog(self):
        """Dialog for asking whether current changes should be saved."""

        if self.saved:
            return True

        msgBox = QMessageBox()
        msgBox.setWindowTitle("Quit")
        msgBox.setText("The document has been modified.")
        msgBox.setInformativeText("Do you want to save your changes?")
        msgBox.setStandardButtons(QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel)
        msgBox.setDefaultButton(QMessageBox.Save)

        # retry save until successful/discard/cancel
        while True:
            val = msgBox.exec()

            if val == QMessageBox.Save:

                if self.saveMap():
                    return True

            elif val == QMessageBox.Discard:
                return True

            # cancel
            else:
                return False


    def textureChanged(self, curr, prev):
        """Handler for signal emitted if a texture was chosen in the texture list."""

        if curr is None:
            return

        name, pixmap = curr.getData()
        self.mapScene.setTexture( Texture(name, pixmap) )


