from PyQt5.QtWidgets import QUndoCommand


class TileCommand(QUndoCommand):
    """Class representing single undoable action."""

    def __init__(self, tileItem, oldPixmap, oldData, parent=None):
        """Constructor

        :param tileItem: TileGraphicsItem
        :param oldPixmap: QPixmap
        :param oldData: Tile

        """
        super().__init__(parent)
        self.item = tileItem

        self.oldPixmap = oldPixmap
        self.oldData = oldData

        self.newPixmap = tileItem.pixmap()
        self.newData = tileItem.data


    def undo(self):
        self.item.setPixmap(self.oldPixmap)
        self.item.data = self.oldData
        self.item.scene().update()


    def redo(self):
        self.item.setPixmap(self.newPixmap)
        self.item.data = self.newData
        self.item.scene().update()


