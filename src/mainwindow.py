from PyQt5.QtWidgets import QAction
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtWidgets import QGroupBox
from PyQt5.QtWidgets import QSizePolicy
from PyQt5.QtWidgets import QToolBar
from PyQt5.QtWidgets import QLayout
from PyQt5.QtWidgets import QUndoStack
from PyQt5.QtWidgets import QListWidget

from PyQt5.QtGui import QFont
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QIcon
from PyQt5.QtGui import QKeySequence

from PyQt5.QtCore import QSize
from PyQt5.QtCore import Qt
from PyQt5.QtCore import pyqtSlot

import sys
import os
import logging

from tilelistitem import TileListItem
from mapdialog import MapDialog

from tile import Texture
from minimap import MiniMap
from mapview import MapView
from utility import Tool
from tilecommand import TileCommand


class MainWindow(QMainWindow):
    """Window for controlling graphical components of the application."""

    def __init__(self, controller):
        """Constructor

        :attr leftPanel: QVBoxLayout
        :attr minimap: MiniMap
        :attr textureList: QListWidget
        :attr mapView: MapView
        :attr controller: MainController, reference to controller for easier access

        """
        super().__init__()

        self.setWindowTitle("Map Editor")
        self.setWindowState(Qt.WindowMaximized)

        self.leftPanel = QVBoxLayout()
        self.minimap = None
        self.textureList = QListWidget()
        self.mapView = None
        self.controller = controller

        central = QWidget()
        self.setCentralWidget(central)
        self.mainLayout = QHBoxLayout()

        self.initMenu()
        self.initMapView()
        self.initToolBar()
        self.initRightPanel()

        central.setLayout(self.mainLayout)


    def initMenu(self):
        menubar = self.menuBar()
        fileMenu = menubar.addMenu("File")

        newIcon = QIcon.fromTheme("document-new", QIcon(self.controller.iconPath + "plus.png"))
        newAction = QAction(newIcon, "New Map", self)
        newAction.setShortcuts(QKeySequence.New)
        newAction.triggered.connect(self.controller.newMap)
        fileMenu.addAction(newAction)

        loadTexIcon = QIcon.fromTheme("document-open", QIcon(self.controller.iconPath + "layers.png"))
        loadTexAction = QAction(loadTexIcon, "Load Textures", self)
        loadTexAction.setShortcuts(QKeySequence(Qt.CTRL + Qt.SHIFT + Qt.Key_O))
        loadTexAction.triggered.connect(self.controller.loadTextures)
        fileMenu.addAction(loadTexAction)

        loadMapIcon = QIcon.fromTheme("document-open", QIcon(self.controller.iconPath + "load.png"))
        loadMapAction = QAction(loadMapIcon, "Load Map", self)
        loadMapAction.setShortcuts(QKeySequence.Open)
        loadMapAction.triggered.connect(self.controller.loadMap)
        fileMenu.addAction(loadMapAction)

        saveMapIcon = QIcon.fromTheme("document-save", QIcon(self.controller.iconPath + "save.png"))
        saveMapAction = QAction(saveMapIcon, "Save Map", self)
        saveMapAction.setShortcuts(QKeySequence.Save)
        saveMapAction.triggered.connect(self.controller.saveMap)
        fileMenu.addAction(saveMapAction)

        saveAsAction = QAction("Save Map As", self)
        saveAsAction.setShortcuts(QKeySequence(Qt.CTRL + Qt.SHIFT + Qt.Key_S))
        saveAsAction.triggered.connect(self.controller.saveAs)
        fileMenu.addAction(saveAsAction)

        fileMenu.addSeparator()

        quitIcon = QIcon.fromTheme("application-exit", QIcon(self.controller.iconPath + "quit.png"))
        quitAction = QAction(quitIcon, "Quit", self)
        quitAction.setShortcuts(QKeySequence.Quit)
        quitAction.triggered.connect(self.close)
        fileMenu.addAction(quitAction)

        editMenu = menubar.addMenu("Edit")
        editMenu.addAction(self.controller.undoAction)
        editMenu.addAction(self.controller.redoAction)


    def initToolBar(self):
        toolbar = QToolBar()

        action = toolbar.addAction("Tile Placer", self.controller.chooseTilePlacer)
        action.setIcon(QIcon(self.controller.iconPath + "cursor.png"))

        action = toolbar.addAction("Tile Eraser", self.controller.chooseTileEraser)
        action.setIcon(QIcon(self.controller.iconPath + "eraser.png"))

        action = toolbar.addAction("Tile Mover", self.controller.chooseViewMover)
        action.setIcon(QIcon(self.controller.iconPath + "move.png"))

        action = toolbar.addAction("Zoom In", self.mapView.zoomIn)
        action.setIcon(QIcon(self.controller.iconPath + "zoom-in.png"))

        action = toolbar.addAction("Zoom Out", self.mapView.zoomOut)
        action.setIcon(QIcon(self.controller.iconPath + "zoom-out.png"))

        action = toolbar.addAction("Center View", self.mapView.centerView)
        action.setIcon(QIcon(self.controller.iconPath + "center.png"))

        self.addToolBar(toolbar)


    def initLeftPanel(self):

        if self.minimap is not None:
            self.minimap.update()
            return

        self.minimap = MiniMap(self.mapView)

        font = QFont("Monospace", 20, QFont.Monospace)
        font.setBold(True)
        label = QLabel("Overview")
        label.setFont(font)
        label.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Maximum)
        label.setAlignment(Qt.AlignTop | Qt.AlignCenter)

        self.leftPanel.setContentsMargins(0, 0, 300, 300)
        self.leftPanel.addWidget(label)
        self.leftPanel.addWidget(self.minimap)

        self.mainLayout.insertLayout(0, self.leftPanel)


    def initMapView(self):
        self.mapView = MapView()
        self.mapView.setScene(self.controller.mapScene)
        mid = QVBoxLayout()
        mid.addWidget(self.mapView)
        self.mainLayout.addLayout(mid)

        self.mapView.mapChanged.connect(self.controller.newChangesMade)


    def initRightPanel(self):
        font = QFont("Monospace", 10, QFont.Monospace)
        font.setBold(True)

        mapLabel = QLabel("Map Size")
        mapLabel.setAlignment(Qt.AlignCenter)
        self.mapInfo = QLabel()
        self.mapInfo.setAlignment(Qt.AlignCenter)
        self.mapInfo.setFont(font)
        mapLabel.setFont(font)
        leftInfoLayout = QVBoxLayout()
        leftInfoLayout.addWidget(mapLabel)
        leftInfoLayout.addWidget(self.mapInfo)

        tileLabel = QLabel("Tile Size")
        tileLabel.setAlignment(Qt.AlignCenter)
        self.tileInfo = QLabel()
        self.tileInfo.setAlignment(Qt.AlignCenter)
        self.tileInfo.setFont(font)
        tileLabel.setFont(font)
        rightInfoLayout = QVBoxLayout()
        rightInfoLayout.addWidget(tileLabel)
        rightInfoLayout.addWidget(self.tileInfo)

        infoLayout = QHBoxLayout()
        infoLayout.addLayout(leftInfoLayout)
        infoLayout.addLayout(rightInfoLayout)
        infoBox = QGroupBox()
        infoBox.setLayout(infoLayout)
        infoBox.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Maximum)

        font = QFont("Monospace", 20, QFont.Monospace)
        font.setBold(True)
        rightPanelHeader = QLabel("Loaded Textures")
        rightPanelHeader.setFont(font)
        rightPanelHeader.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
        rightPanelHeader.setAlignment(Qt.AlignTop)


        deleteButton = QPushButton()
        deleteButton.setIcon( QIcon( QPixmap(self.controller.resPath + "icons/delete.png") ) )
        deleteButton.setToolTip("remove unused textures")
        deleteButton.pressed.connect(self.controller.clearLoadedTextures)

        self.textureList.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Minimum)
        self.textureList.setIconSize( QSize(64, 64) )
        font = QFont("Monospace", 10, QFont.Monospace)
        font.setBold(True)
        self.textureList.setFont(font)


        headerLayout = QHBoxLayout()
        headerLayout.addWidget(rightPanelHeader)
        headerLayout.addWidget(deleteButton)

        rightPanelLayout = QVBoxLayout()
        rightPanelLayout.addLayout(headerLayout)
        rightPanelLayout.addWidget(self.textureList)
        rightPanelLayout.addWidget(infoBox)

        self.mainLayout.addLayout(rightPanelLayout)

        self.textureList.currentItemChanged.connect(self.controller.textureChanged)


    def closeEvent(self, event):
        """Handler for signal generated when the window is trying to close."""

        logging.info("close event")

        if self.controller.askSaveDialog():
            event.accept()
        else:
            event.ignore()




