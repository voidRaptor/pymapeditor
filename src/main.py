from PyQt5.QtWidgets import QApplication
import sys

from maincontroller import MainController


if __name__ == "__main__":
    app = QApplication(sys.argv)

    ctrl = MainController()
    ctrl.window.show()

    app.exec()

