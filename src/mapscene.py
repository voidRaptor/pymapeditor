from PyQt5.QtWidgets import QGraphicsScene
from PyQt5.QtWidgets import QGraphicsView

from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QColor

from PyQt5.QtCore import QSize
from PyQt5.QtCore import Qt
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtCore import pyqtSignal

from copy import deepcopy

from tilegraphicsitem import TileGraphicsItem
from tile import Tile
from tile import Texture
from utility import Tool
from utility import showInfoMessage
from utility import showErrorMessage


class MapScene(QGraphicsScene):
    """Container for tiles in the map, handles the data operations related to tiles."""

    mapChanged = pyqtSignal(TileGraphicsItem, QPixmap, Tile)


    def __init__(self, controller):
        """Constructor

        :attr currentTexture: Texture, currently selected placeable tile type
        :attr mapSize: (int, int), width and height of the map
        :attr cellSize: (int, int), width and height of cell representing a tile
        :attr defaultPixmap: QPixmap, pixmap of Tiles, of which type is None

        """
        super().__init__()

        self.currentTexture = None

        self.mapSize = (10, 10)
        self.cellSize = (64, 64)

        self.defaultPixmap = QPixmap( QSize(self.cellSize[0], self.cellSize[1]) )
        self.defaultPixmap.fill( QColor(200, 200, 200, 255) )

        self.controller = controller


    def createEmptyGrid(self):
        """Create grid of tiles with default texture using previously set tile and map dimensions."""

        self.clear()

        w = self.cellSize[0]
        h = self.cellSize[1]

        view = self.views()[0]
        xOff = (view.rect().width()  - w * self.mapSize[0]) * 0.5
        yOff = (view.rect().height() - h * self.mapSize[1]) * 0.5


        for j in range(self.mapSize[1]):

            for i in range(self.mapSize[0]):
                x = i * self.cellSize[0]
                y = j * self.cellSize[1]

                item = TileGraphicsItem(i, j, self.defaultPixmap)
                self.addItem(item)
                item.setPos(x + xOff, y + yOff)

        self.setSceneRect(self.itemsBoundingRect())


    def createMap(self, tiles, texDb):
        """Create map from tiles.

        :param tiles: [Tile,], list of tiles to render
        :attr texDb: TextureDatabase, used to retrieve pixmaps of tile types

        """
        self.clear()

        # assuming order of tiles has not been changes,
        # indexing is from 0
        maxX = tiles[-1].x + 1
        maxY = tiles[-1].y + 1

        self.mapSize = (maxX, maxY)

        w = self.cellSize[0]
        h = self.cellSize[1]

        # translation to correct position
        view = self.views()[0]
        xOff = (view.rect().width()  - w * self.mapSize[0]) * 0.5
        yOff = (view.rect().height() - h * self.mapSize[1]) * 0.5

        for j in range(self.mapSize[1]):

            for i in range(self.mapSize[0]):
                x = i * w
                y = j * h

                tiledata = tiles[j * self.mapSize[0] + i]
                typestr = tiledata.type

                if typestr is None:
                    pixmap = self.defaultPixmap
                else:
                    pixmap = texDb.getPixmap(typestr)

                item = TileGraphicsItem(i, j, pixmap)
                item.data = tiledata

                self.addItem(item)
                item.setPos(x + xOff, y + yOff)

        self.setSceneRect(self.itemsBoundingRect())


    def setMapData(self, mapW, mapH):
        self.mapSize = (mapW, mapH)


    def tileAction(self, event, tileItem):
        """Handler for tile click event.

        :param event: QMousePressEvent
        :param tileItem: TileGraphicsItem, tile to perform the action on

        """
        # user is currently dragging
        if self.views()[0].dragMode() != QGraphicsView.NoDrag:
            event.ignore()
            return

        # tile action is only for left mouse button
        if event.button() != Qt.LeftButton:
            return

        if event.modifiers() == Qt.ShiftModifier:
            self.eraseTile(tileItem)
        else:
            self.doToolAction(tileItem)



    def doToolAction(self, tileItem):
        """Selects action based on current tool.

        :param tileItem: TileGraphicsItem, tile to perform the action on

        """
        tool = self.controller.chosenTool
        self.views()[0].setDragMode(QGraphicsView.NoDrag)

        if tool == Tool.PLACER:
            self.placeTile(tileItem)

        elif tool == Tool.ERASER:
            self.eraseTile(tileItem)

        elif tool != Tool.MOVER:
            showErrorMessage("unknown tool")

            # NOTE: case Tool.MOVER is handled in MapView, since it changes the
            # view, not the data of tile


    def getTiles(self):
        """Retrieve list of tiles

        :return: [Tile,], list of Tile objects

        """
        tiles = []
        tileItems = self.items()

        for item in tileItems:
            tiles.append(item.data)

        return tiles


    @pyqtSlot(Texture)
    def setTexture(self, tex):
        """Set texture as the placeable."""

        self.currentTexture = tex


    def getUsedTextures(self):
        """Generate dict of used textures.

        :return {str: QPixmap}

        """
        used = {}

        for item in self.items():

            if item.data.type not in used and item.data.type is not None:
                used[item.data.type] = item.pixmap()


        return used


    def eraseTile(self, tileItem):
        """Erase specific tile, i.e change its texture to defaultPixmap.

        :param tileItem: TileGraphicsItem, the chosen tile

        """
        if tileItem is None:
            logging.info("tileitem is none")
            return

        if self.defaultPixmap is None:
            showErrorMessage("no default texture set")
            return

        oldPixmap = tileItem.pixmap()
        oldData = deepcopy(tileItem.data)

        tileItem.setPixmap(self.defaultPixmap)
        tileItem.data.type = None

        # dont emit if erase was used on empty tile
        if oldData.type is not None:
            self.mapChanged.emit(tileItem, oldPixmap, oldData)


    def placeTile(self, tileItem):
        """Place a texture on specific tile, i.e change its texture to chose one.

        :param tileItem: TileGraphicsItem, the chosen tile

        """
        if tileItem is None:
            logging.info("tileitem is none")
            return

        if self.currentTexture is None:
            text = "No texture set.\n\nTip: File->Load Textures\nand click on preferred texture"
            showInfoMessage(text)
            return

        oldPixmap = tileItem.pixmap()
        oldData = deepcopy(tileItem.data)

        tileItem.setPixmap(self.currentTexture.pixmap)
        tileItem.data.type = self.currentTexture.name

        # dont emit if same tile is placed
        if oldData.type != tileItem.data.type:
            self.mapChanged.emit(tileItem, oldPixmap, oldData)


    def requestContextMenu(self, event, tileItem):
        """Handler for TileGraphicItem's contextMenuEvent.

        :param event: QContextMenuEvent
        :param tileItem: TileGraphicsItem

        Relays the request to the view object, which is responsible for graphics.

        """

        # allow only click without modifier keys (e.g. shift)
        if event.modifiers() != Qt.NoModifier:
            event.ignore()
            return

        self.views()[0].openTileDialog(event.pos(), tileItem)

