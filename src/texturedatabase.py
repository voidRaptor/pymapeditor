from PyQt5.QtGui import QPixmap

import logging


class Entry():
    """An entry in texturedatabase."""

    def __init__(self, name, path, pixmap):
        self.name = name
        self.path = path
        self.pixmap = pixmap


class TextureDatabase():
    """Loads and stores for different tile textures."""

    def __init__(self):
        """Constuctor

        :attr textures: {typename: Entry}

        """
        self.textures = {}


    def loadTile(self, path):
        """Load tile image file from path.

        :param path: str
        :return (str, QPixmap)

        """
        begin = path.rfind("/") + 1
        end = path.rfind(".")

        name = path[begin:end]
        pixmap = QPixmap(path)

        realName = self.add(name, pixmap, path)

        if realName is None:
            return (None, None)

        return (realName, pixmap)


    def add(self, name, pixmap, path=None):
        """Add a pixmap and its filepath with name (key)  to database.


        :param name: str, key for entry
        :param pixmap: QPixmap, payload
        :param path: str, filepath where pixmap was loaded from

        :return: str, key used to store the pixmap


        If a pixmap with given name already exists, but their file paths differ,
        a shuffix will be added to the name. That's also why the key is returned.

        """
        index = 0
        key = name

        while key in self.textures:

            if self.textures[key].path == path:
                logging.info("tile already exists")
                return None

            index += 1
            key = name + "_" + str(index)

        self.textures[key] = Entry(name, path, pixmap)

        return key


    def contains(self, name):
        """Check whether the database contains a texture with given name.

        :param name: str, texture name to look for

        :return: bool
            True if contains or name is None (tile with no type)
            False else

        """
        # for empty tiles
        if name is None:
            return True

        return name in self.textures


    def remove(self, name):
        """Remove texture with given name.

        :param name: str, texture name to look for

        """
        self.textures.pop(name)


    def getName(self, pixmap):
        """Retrieve name of texture containing the given pixmap.

        :param pixmap: QPixmap, pixmap to look for

        :return: str if found, None otherwise

        """
        for key in self.textures:

            if self.textures[key] == pixmap:
                return key

        return None


    def getPixmap(self, name):
        """Retrieve texture's pixmap with given name.

        :param name: str, texture name to look for

        :return: QPixmap

        """
        return self.textures[name].pixmap


    def getPath(self, name):
        """Retrieve the path used to load the texture with given name.

        :param name: str, texture name to look for

        :return: str

        """
        return self.textures[name].path


