from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QFormLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QCheckBox
from PyQt5.QtWidgets import QDialog
from PyQt5.QtWidgets import QSpinBox

from tile import Tile


class TileDialog(QDialog):
    """Dialog for editing selected tile's attributes."""

    def __init__(self, tile, parent=None):
        """Constructor

        :param tile: Tile, the chosen tile
        :param parent: QWidget

        """
        super().__init__(parent)

        self.setWindowTitle("Tile Attributes")

        self.tile = tile

        self.traverseSpin = QSpinBox()
        self.traverseSpin.setMinimum(0)
        self.traverseSpin.setMaximum(3)
        self.traverseSpin.setValue(self.tile.traversability)

        self.destroyCheck = QCheckBox("Destroyable")
        self.destroyCheck.setChecked(self.tile.destroyable)

        okButton = QPushButton("Ok")

        tileType = tile.type

        if tileType is None:
            tileType = "No type"

        layout = QFormLayout()
        layout.addRow(QLabel("Type:"), QLabel(tileType))
        layout.addRow(self.traverseSpin, QLabel("Traversability\n0: non-traversable\n3: normal"))
        layout.addRow(self.destroyCheck)
        layout.addRow(okButton)
        self.setLayout(layout)

        okButton.pressed.connect(self.accept)

        self.show()


    def accept(self):
        """Handler for closing event.

        Set values of dialog elements as tile's attributes' values.

        """
        self.tile.traversability = self.traverseSpin.value()
        self.tile.destroyable = self.destroyCheck.isChecked()
        super().accept()


