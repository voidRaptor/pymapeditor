from PyQt5.QtWidgets import QMessageBox
from enum import Enum, auto

import logging


class Tool(Enum):
    """Enum for different tool options."""
    PLACER = auto()
    ERASER = auto()
    MOVER = auto()


def getFileName(path):
    """Extract file name from given path.

    :param path: str

    :return: str

    """
    begin = path.rfind("/") + 1
    return path[begin:]



def showErrorMessage(text, pos=None, parent=None):
    """Open dialog for showing error messages to user, also log the message to the logfile.

    :param text: str, error message to display
    :param pos: QPoint, position for dialog, by default roughly center of screen
    :param parent: QWidget

    """
    logging.error(text)
    openMessageBox("Error", text, pos, parent)


def showInfoMessage(text, pos=None, parent=None):
    """Open dialog for showing info messages to user, also log the message to the logfile.

    :param text: str, info message to display
    :param pos: QPoint, position for dialog, by default roughly center of screen
    :param parent: QWidget

    """
    logging.info(text)
    openMessageBox("Info", text, pos, parent)


def openMessageBox(title, text, pos=None, parent=None):
    """Open message box with given parameters.

    :param title: str, title for message box dialog
    :param text: str, text to display
    :param pos: QPoint, position for dialog, by default roughly center of screen
    :param parent: QWidget

    """
    msgBox = QMessageBox()
    msgBox.setText(text)
    msgBox.setWindowTitle(title)
    msgBox.exec()

