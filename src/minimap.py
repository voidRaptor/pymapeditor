from PyQt5.QtWidgets import QWidget

from PyQt5.QtGui import QColor
from PyQt5.QtGui import QPen
from PyQt5.QtGui import QPainter
from PyQt5.QtGui import QBrush
from PyQt5.QtGui import QPolygon

from PyQt5.QtCore import QTimer
from PyQt5.QtCore import QPoint
from PyQt5.QtCore import QPointF
from PyQt5.QtCore import QSizeF
from PyQt5.QtCore import QRectF
from PyQt5.QtCore import Qt

import logging

class MiniMap(QWidget):
    """Widget for showing, which part of the graphicsscene is actually viewed."""

    def __init__(self, view, parent=None):
        """Constructor

        :param view: MapView
        :param parent: QWidget

        :attr view: MapView, refernce to the view for convenience
        :attr mapSize: (int, int), size of map in tiles
        :attr tileSize: (int, int), size of tiles in pixels
        :attr updateTimer: timer for updating the minimap periodically

        """
        super().__init__(parent)

        #self.setMouseTracking(True)
        self.view = view

        self.mapSize = None
        self.tileSize = None

        self.updateTimer = QTimer()
        self.updateTimer.setInterval(250)
        self.updateTimer.timeout.connect(self.update)


    def update(self):
        """Override for making sure minimap size is correctly set constantly."""

        self.setSize()
        super().update()


    def setSize(self):
        """Set width to MAX_SIZE and scale height accordingly."""

        MAX_SIZE = 400
        w = MAX_SIZE
        h = MAX_SIZE * self.view.size().height() / self.view.size().width()

        #self.setFixedSize(w, h)
        self.resize(w, h)


    def paintEvent(self, event):
        """Draw representation of whole map and viewed map.

        :param: QPaintEvent

        """
        super().paintEvent(event)

        if self.tileSize is None or self.mapSize is None:
            return

        LINEWIDTH = 2
        brush = QBrush( QColor(200, 200, 200, 255) )
        # prevent missing pixels in left corners
        pen = QPen(brush, LINEWIDTH, Qt.SolidLine, Qt.SquareCap, Qt.MiterJoin)

        painter = QPainter(self)
        painter.setPen(pen)

        # outline
        painter.drawRect(0, 0, self.size().width(), self.size().height())

        brush = QBrush( QColor(0, 0, 200, 255) )
        pen = QPen(brush, LINEWIDTH, Qt.SolidLine, Qt.SquareCap, Qt.MiterJoin)
        painter.setPen(pen)

        # polygon is bit more sensitive to view zoom level changes than rectangle
        polygon = self.getViewPolygon()
        painter.drawPolygon(polygon, 4)


    def getViewPolygon(self):
        """Get currently visible area of graphicsscene as polygon."""

        viewRect = self.view.mapToScene(self.view.viewport().geometry()).boundingRect()

        wScale = self.size().width()  / (self.mapSize[0] * self.tileSize[0])
        hScale = self.size().height() / (self.mapSize[1] * self.tileSize[1])

        x = (viewRect.x() - self.view.scene().sceneRect().x()) * wScale
        y = (viewRect.y() - self.view.scene().sceneRect().y()) * hScale
        w = viewRect.width()  * wScale
        h = viewRect.height() * hScale

        p0 = QPoint(x, y)
        p1 = QPoint(x + w, y)
        p2 = QPoint(x + w, y + h)
        p3 = QPoint(x, y + h)

        def limitTo(point, limit):

            if point.x() > limit.width():
                point.setX( limit.width() )

            if point.y() > limit.height():
                point.setY( limit.height() )

            if point.x() < limit.x():
                point.setX( limit.x() )

            if point.y() < limit.y():
                point.setY( limit.y() )

            return point

        p0 = limitTo( p0, QRectF( QPointF(0.0, 0.0), QSizeF(self.size()) ) )
        p1 = limitTo( p1, QRectF( QPointF(0.0, 0.0), QSizeF(self.size()) ) )
        p2 = limitTo( p2, QRectF( QPointF(0.0, 0.0), QSizeF(self.size()) ) )
        p3 = limitTo( p3, QRectF( QPointF(0.0, 0.0), QSizeF(self.size()) ) )

        polygon = QPolygon(4)
        polygon.setPoint(0, p0)
        polygon.setPoint(1, p1)
        polygon.setPoint(2, p2)
        polygon.setPoint(3, p3)

        return polygon


    def setMapData(self, mapSize, tileSize):
        """Set map's and tiles' dimensions, also start update timer.

        :param mapSize: (int, int), size of map in tiles
        :param tileSize: (int, int), size of tile in pixels

        """
        self.mapSize = mapSize
        self.tileSize = tileSize

        self.setSize()

        if not self.updateTimer.isActive():
            self.updateTimer.start()


    def mousePressEvent(self, event):
        """Handler for mouse click on minimap FOR FUTURE USAGE AND DEBUGGING

        :param event: QMousePressEvent

        """
        viewRect = self.view.mapToScene(self.view.viewport().geometry()).boundingRect()
        logging.info(viewRect)

