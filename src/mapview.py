from PyQt5.QtWidgets import QGraphicsView

from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QCursor
from PyQt5.QtGui import QMouseEvent

from PyQt5.QtCore import QPoint
from PyQt5.QtCore import Qt
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtCore import QEvent

from copy import deepcopy

import logging

from tilegraphicsitem import TileGraphicsItem
from tile import Tile
from tiledialog import TileDialog
from utility import Tool



class MapView(QGraphicsView):
    """Graphical representation of the map.


    NOTE: due to graphics view architecture, it's much easier not to separate
    data from graphics.
    """

    mapChanged = pyqtSignal(TileGraphicsItem, QPixmap, Tile)


    def __init__(self, parent=None):
        """Constructor

        :param parent: QWidget

        :attr dialogExists: bool, ensures there's only 1 dialog open

        """
        super().__init__(parent)

        self.setMouseTracking(True)
        self.setAlignment(Qt.AlignCenter)

        self.dialogExists = False


    def openTileDialog(self, mousePos, tileItem):
        """Open dialog to edit the chosen tile's attributes."""

        logging.info("dialog")


        if self.dialogExists:
            logging.info("dialog already exists")
            return

        self.dialogExists = True

        if tileItem is None:
            return

        # for drawing indication of selection
        tileItem.selected = True

        tileItem.update()

        oldData = deepcopy(tileItem.data)
        dialog = TileDialog(tileItem.data)

        OFFSET = QPoint(40, 0)
        globalPos = self.mapToGlobal(QCursor.pos() + OFFSET)

        dialog.move(self.mapFromGlobal(globalPos))
        dialog.exec()

        tileItem.selected = False
        tileItem.update()
        self.dialogExists = False


        changed = False

        if oldData.traversability != tileItem.data.traversability:
            changed = True

        if oldData.destroyable != tileItem.data.destroyable:
            changed = True


        if changed:
            logging.info("data changed")
            # pixmap doesnt change
            self.mapChanged.emit(tileItem, tileItem.pixmap(), oldData)


    def wheelEvent(self, event):
        """Override for QGraphicsView.

        :param event: QWheelEvent

        """
        # horizontal scroll
        if event.modifiers() == Qt.ShiftModifier:
            self.horizontalScrollBar().event(event)

        # vertical scroll
        elif event.modifiers() == Qt.NoModifier:
            self.verticalScrollBar().event(event)

        # zoom with mouse as center point
        elif event.modifiers() == Qt.ControlModifier:
            anchor = self.transformationAnchor()
            self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)

            if event.angleDelta().y() > 0:
                self.zoomIn()
            else:
                self.zoomOut()

            self.setTransformationAnchor(anchor)


        event.accept()


    def mousePressEvent(self, event):
        """Override for QGraphicsView.

        :param event: QMousePressEvent

        """

        # enable view drag with mover tool
        if self.parent().parent().controller.chosenTool == Tool.MOVER and\
            event.button() == Qt.LeftButton and\
            event.modifiers() == Qt.NoModifier:

            self.setDragMode(QGraphicsView.ScrollHandDrag)
        else:
            self.setDragMode(QGraphicsView.NoDrag)


        # drag with right mouse button
        if event.button() == Qt.RightButton and event.modifiers() == Qt.ShiftModifier:
            self.setDragMode(QGraphicsView.ScrollHandDrag)

            # emulate left mouse click, since qt's view drag button cannot be changed
            pressEvent = QMouseEvent(QEvent.GraphicsSceneMousePress,
                                     event.pos(),
                                     Qt.LeftButton,
                                     Qt.LeftButton,
                                     Qt.NoModifier)

            super().mousePressEvent(pressEvent)

        else:
            super().mousePressEvent(event)


    def mouseReleaseEvent(self, event):
        """Override for QGraphicsView."""

        self.setDragMode(QGraphicsView.NoDrag)


    def zoomIn(self):
        """Zoom into the view."""

        self.scale(1.1, 1.1)


    def zoomOut(self):
        """Zoom out of the view."""

        self.scale(0.9, 0.9)


    def centerView(self):
        """Center the view."""

        # find center coordinates in pixels and translate to screen coordinates
        rect = self.scene().sceneRect()
        x = rect.x() + int(self.scene().mapSize[0] * self.scene().cellSize[0] * 0.5)
        y = rect.y() + int(self.scene().mapSize[1] * self.scene().cellSize[1] * 0.5)
        self.centerOn( QPoint(x, y) )


