from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import QObject


class Texture(QObject):
    """Used for passing tile data via signals """

    def __init__(self, name, pixmap):
        """Constructor

        :param name: str
        :param pixmap: QPixmap

        """
        super().__init__()
        self.name = name
        self.pixmap = pixmap


class Tile():
    """Contains the data of a tile."""

    def __init__(self, x, y, typestr=None):
        """Constructor

        :param x: int, index of the tile on x axis
        :param y: int, index of the tile on y axis
        :param typestr: str, type of the tile

        :attr traversability: int, how hard it's to travel through the tile
            0 non-traversable
            1 very slow
            2 almost normal
            3 normal

        :attr destroyable: bool, whether the tile can be destroyed
        :attr items: [], items put on the tile NOTE: not used

        Other attributes than x, y and type are set using TileDialog. They don't
        affect the application, but are significant for the game using the map.
        """
        self.x = x
        self.y = y
        self.type = typestr
        self.traversability = 3
        self.destroyable = False

        self.items = []


    def __str__(self):
        """Print object data using str()."""

        return f"{self.x}, {self.y}, {self.type}, {self.traversability}, {self.destroyable}, {self.items}"


    def __repr__(self):
        """Print data of objects in a list using str()."""

        return f"<{str(self)}>"

