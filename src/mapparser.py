from operator import attrgetter
from distutils import util
from tile import Tile

import utility


def writeMap(path, tiles):
    """Write data of given tiles to given filepath."""

    tiles = sorted(tiles, key=attrgetter("x", "y"))

    with open(path, "w") as f:

        # x:y:z:type:traversable:destroyable:item1:item2:item3...
        for tile in tiles:
            items = ",".join(tile.items)

            if tile.type is None:
                typestr = "default"
            else:
                typestr = tile.type

            f.write(f"{tile.x}:{tile.y}:{typestr}:{tile.traversability}:{tile.destroyable}:{items}\n")



def readMap(path):
    """Read tiles' data from given file into list of Tile objects."""

    tiles = []

    try:
        f = open(path, "r")

        for line in f:
            values = line.rstrip("\n").split(":")

            tile = Tile(int(values[0]), int(values[1]))

            if values[2] == "default":
                tile.type = None
            else:
                tile.type = values[2]

            tile.traversability = int(values[3])
            tile.destroyable = bool( util.strtobool(values[4]) )
            tile.items = values[-1].split(",")

            if len(tile.items) == 1 and tile.items[0] == "":
                tile.items = []

            tiles.append(tile)

    except:

        if path != "":
            utility.showErrorMessage(f"could not open file \"{path}\"")

        return []


    f.close()


    tiles = sorted(tiles, key=attrgetter("y", "x"))

    return tiles


