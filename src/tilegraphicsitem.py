from PyQt5.QtWidgets import QGraphicsPixmapItem
from PyQt5.QtWidgets import QWidget

from PyQt5.QtGui import QColor
from PyQt5.QtGui import QPen
from PyQt5.QtGui import QBrush

from PyQt5.QtCore import Qt

from tile import Tile


class TileGraphicsItem(QGraphicsPixmapItem):
    """Graphical representation of a tile

    For easier access in the qt's graphics view architecture, also
    other data about tile has been included. It's also safer to include tile
    indices and other data to the graphics items, since in some cases tile search
    using pixel coordinates turned out to be unreliable. This is mostly due to
    the containers and layouts shifting all tiles random amount from the center,
    when new elements were added to layouts or window size were changed.
    """

    def __init__(self, x, y, pixmap):
        """Constructor

        :param x: int, index of tile on x-axis
        :param y: int, index of tile on y-axis
        :param pixmap: QPixmap, pixmap for tile

        :attr data: Tile, contains the tile's attributes
        :attr selected: bool, whether tile selection indicator is shown for tile

        """
        super().__init__(pixmap)

        self.data = Tile(x, y)
        self.selected = False


    def paint(self, painter, option, widget):
        """Implements special rendering: draws indicator rectangle if selected.

        :param painter: QPainter
        :param option: QStyleOptionGraphicsItem
        :param widget: QWidget


        """
        super().paint(painter, option, widget)

        if self.selected:
            LINEWIDTH = 4
            brush = QBrush( QColor(255, 0, 255, 255) )
        else:
            LINEWIDTH = 2
            brush = QBrush( QColor(0, 0, 0, 255) )

        pen = QPen(brush, LINEWIDTH)
        painter.setPen(pen)

        rect = self.boundingRect()
        x = rect.x() + LINEWIDTH * 0.5
        y = rect.y() + LINEWIDTH * 0.5
        w = rect.width() - LINEWIDTH * 0.5
        h = rect.height() - LINEWIDTH * 0.5

        # clockwise from top edge
        painter.drawLine(x, y, x + w, y);
        painter.drawLine(x + w, y, x + w, y + h);
        painter.drawLine(x + w, y + h, x, y + h);
        painter.drawLine(x, y + h, x, y)


    def mousePressEvent(self, event):
        """Handler for mouse press event, relay event to scene.

        :param event: QMousePressEvent

        """
        self.scene().tileAction(event, self)


    def contextMenuEvent(self, event):
        """Handler for context menu event, relay event to scene.

        :param event: QContextMenuEvent

        """
        self.scene().requestContextMenu(event, self)


