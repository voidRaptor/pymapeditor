from PyQt5.QtWidgets import QListWidgetItem
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSize


class TileListItem(QListWidgetItem):
    """Represents entry in list of available tile textures."""

    def __init__(self, name, pixmap, parent=None):
        """Constructor

        :param name: str, name of the tile texture in TextureDatabase
        :param pixmap: QPixmap, icon for entry
        :param parent: QListWidget

        :attr selected: bool, whether the entry has been selected
        :attr name: str, name of the tile texture in TextureDatabase

        """
        super().__init__( QIcon(pixmap), name, parent)

        self.selected = False
        self.name = name


    def select(self, b):
        """Set as selected or unselected.

        :param b: bool

        """
        self.selected = b


    def getData(self):
        """Retrieve name and pixmap scaled to 64x64 pixels.

        :return: (str, QPixmap)

        """
        return (self.name, self.icon().pixmap(QSize(64, 64)))

