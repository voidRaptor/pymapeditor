# Map Editor

Simple map editor for 2D tile based games


![main view with tile attribute dialog](docs/image1.png)
**Attributes for specific til can be edited by opening a dialog with right-click.**


![map of 100x100 tiles](docs/image2.png)
**When drawing larger maps, overview indicates the shown part of the map.**


## How to run

NOTE: Following intstructions are for linux. On windows, venv/bin/activate ->
venv/scripts/activate, but everything else should work just fine.


### Install dependencies for app and tests

In project root:
```
python3 -m venv venv
. venv/bin/activate
pip install -e .
pip install -e .[test]
```


### Run

In project root:
```
. venv/bin/activate
python src/main.py
```

### Run tests

In project root:
```
. venv/bin/activate
python -m pytest
```


## Resources

The application uses icons from 2 sources: icons in in {project root}/res/icons
are from [gimp](https://gitlab.gnome.org/GNOME/gimp), and rest are provided by
(py)Qt.

